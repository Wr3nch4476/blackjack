﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardValue{
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King
}

public enum CardSuite{
    Hearts,
    Diamonds,
    Spades,
    Clubs
}

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class Card : ScriptableObject
{
    public CardValue cardNumber;
    public CardSuite cardSuite;

    public Sprite cardFace;

}


