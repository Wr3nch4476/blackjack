﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dealer : MonoBehaviour {
    public static Dealer instance;
    
    [SerializeField] public List<Card> cardsNoShuffle = new List<Card> ();

    public int count;
    private void Awake () {
        instance = this;
    }

    public void DealHouseCards () {
        //house
        count = Random.Range (0, cardsNoShuffle.Count - 1);
        House.instance.UpdateHouseDeck (count);

    }

    public void DealPlayerCards () {
        //player
        count = Random.Range (0, cardsNoShuffle.Count - 1);
        Player.instance.UpdatePlayerDeck (count);
        
        if (Game.instance._doneDealingCards) {
            Game.instance._firstTurn = false;
        }
        

    }

}