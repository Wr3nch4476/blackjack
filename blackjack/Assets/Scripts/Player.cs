﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public static Player instance;
    [SerializeField] public List<Card> playerInv = new List<Card> ();

    [SerializeField] public List<GameObject> playerCardInv = new List<GameObject> ();

    [SerializeField] public RectTransform playerCards;

    [SerializeField] public GameObject newPlayerCard;
    public static int playerScore;
    [SerializeField] int editorPlayerScore;

    private void Awake () {
        instance = this;
    }

    public void UpdatePlayerDeck (int index) {

        Card cardPlaceholder = Dealer.instance.cardsNoShuffle[index];
        playerInv.Add (cardPlaceholder);

        StartCoroutine (PauseABit ());

        DisplayCard CardSprite = newPlayerCard.GetComponent<DisplayCard> ();
        CardSprite.card = cardPlaceholder;

        StartCoroutine (PauseABit ());

        GameObject tempGameobject = Instantiate (newPlayerCard, playerCards);
        playerCardInv.Add (tempGameobject);

        updatePlayerScore ();

    }

    IEnumerator PauseABit () {

        yield return new WaitForSeconds (0.2f);

    }

    private void Update () {
        editorPlayerScore = playerScore;

        if (playerScore > 21) {
            Game.instance._playerDone = true;
        }
    }
    public void updatePlayerScore () {

        int numberOfAces = 0;

        playerScore = 0;

        for (int i = 0; i < playerInv.Count; i++) {

            CardValue getCardValue = playerInv[i].cardNumber;

            if (getCardValue == CardValue.Ace) {
                ++numberOfAces;
            }
            if (getCardValue == CardValue.Two) {
                playerScore += 2;
            }
            if (getCardValue == CardValue.Three) {
                playerScore += 3;
            }
            if (getCardValue == CardValue.Four) {
                playerScore += 4;
            }
            if (getCardValue == CardValue.Five) {
                playerScore += 5;
            }
            if (getCardValue == CardValue.Six) {
                playerScore += 6;
            }
            if (getCardValue == CardValue.Seven) {
                playerScore += 7;
            }
            if (getCardValue == CardValue.Eight) {
                playerScore += 8;
            }
            if (getCardValue == CardValue.Nine) {
                playerScore += 9;
            }
            if (getCardValue == CardValue.Ten) {
                playerScore += 10;
            }
            if (getCardValue == CardValue.Jack) {
                playerScore += 10;
            }
            if (getCardValue == CardValue.Queen) {
                playerScore += 10;
            }
            if (getCardValue == CardValue.King) {
                playerScore += 10;
            }

            for (int j = 0; j < numberOfAces; j++) {
                if (j < numberOfAces - 1) {
                    playerScore += 1;
                } else {
                    if (playerScore < 11) {
                        playerScore += 11;
                    } else {
                        playerScore += 1;
                    }
                }
            }

            Game.instance._playerScore = playerScore;
        }

    } // end of updatePlayerScore

    public void PlayerStand () {

        Game.instance._playerDone = true;
        Game.instance._playerTurn = false;
        Game.instance._houseTurn = true;
        Game.instance._firstTurn = false;

    }

    public void DestroyCards () {

        for (int i = 0; i < playerCardInv.Count; i++) {
            GameObject tempCard = playerCardInv[i];
            Destroy (tempCard);

        }

    }

}