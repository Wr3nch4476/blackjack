﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour {

    public static Game instance;

    public bool _restartComplete;
    [Header ("Buttons")]
    [SerializeField] GameObject startButton;
    [SerializeField] GameObject standButton;
    [SerializeField] GameObject hitButton;

    [Header ("Logic")]
    public int _playerScore;
    public int _houseScore;
    public bool _houseDone;
    public bool _playerDone;

    public bool _houseTurn;
    public bool _playerTurn;

    public bool _firstTurn;
    public bool _pointsNotGiven;
    public bool _doneDealingCards;

    [Header ("Score holders")]
    [SerializeField] Text houseTextPlaceHolder;
    [SerializeField] Text playerTextPlaceHolder;
    [SerializeField] Text playerMoney;

    [SerializeField] GameObject _playerScorePlaceHolder;
    [SerializeField] GameObject _houseScorePlaceHolder;
    [SerializeField] GameObject _playerMoneyPlaceHolder;

    public int playerMoneyCoins;

    [SerializeField] int winOrLoseAmount = 15;

    public bool cardsGiven = false;

    [Header ("Winning Text")]
    [SerializeField] GameObject playerWonText;
    [SerializeField] GameObject houseWonText;

    private void Awake () {
        instance = this;
    }

    public void Exit () {
        Debug.Log("Im out Chief");
        Application.Quit();
    }
    private void Start () {
        houseTextPlaceHolder.text = _houseScore.ToString ();
        playerTextPlaceHolder.text = _playerScore.ToString ();
        playerMoneyCoins = 100;
        playerMoney.text = "Money: " + playerMoneyCoins.ToString ();
        _playerTurn = true;
        _houseTurn = false;
        _pointsNotGiven = true;
        _firstTurn = true;
        _doneDealingCards = false;
        _restartComplete = true;
    }

    private void Update () {
        houseTextPlaceHolder.text = _houseScore.ToString ();
        playerTextPlaceHolder.text = _playerScore.ToString ();
        playerMoney.text = "Money: " + playerMoneyCoins.ToString ();

        CheckScore ();

        if (_playerTurn) {
            _houseTurn = false;
        } else if (_houseTurn) {
            _playerTurn = false;
        }
    }

    public void StartGame () {

        startButton.SetActive (false);
        hitButton.SetActive (true);
        standButton.SetActive (true);
        _playerMoneyPlaceHolder.SetActive (true);
        _playerScorePlaceHolder.SetActive (true);
        _houseScorePlaceHolder.SetActive (true);

        StartCoroutine (CheckGameStatus ());

    }

    IEnumerator CheckGameStatus () {

        yield return new WaitForSeconds (2f);

        if (_restartComplete) {
            ScoreUpdater ();
        }

        StartCoroutine (DealingBeginingCards ());

        _doneDealingCards = true;

        Debug.Log ("Started Dealing Cards");

    }

    IEnumerator DealingBeginingCards () {

        Dealer.instance.DealPlayerCards ();

        yield return new WaitForSeconds (1f);

        Dealer.instance.DealHouseCards ();

        yield return new WaitForSeconds (1f);

        Dealer.instance.DealPlayerCards ();

        yield return new WaitForSeconds (1f);

        Dealer.instance.DealHouseCards ();

        yield return new WaitForSeconds (1f);

        cardsGiven = true;

        _houseTurn = false;
        _playerTurn = false;
        _firstTurn = true;

    }

    public void ScoreUpdater () {
        if (cardsGiven) {
            Player.instance.updatePlayerScore ();
            House.instance.updateHouseScore ();
        }
    }

    private void CheckScore () {
        if (_restartComplete) {
            if (_firstTurn) {
                if (_houseScore == 21) {
                    playerMoneyCoins -= winOrLoseAmount;
                    Debug.Log ("House Wins");
                    houseWonText.SetActive (true);
                    StartCoroutine (ResetGame ());
                    _pointsNotGiven = false;
                    _restartComplete = false;
                } else if (_playerScore == 21) {
                    playerMoneyCoins += winOrLoseAmount;
                    Debug.Log ("player Wins");
                    playerWonText.SetActive (true);
                    StartCoroutine (ResetGame ());
                    _pointsNotGiven = false;
                    _restartComplete = false;
                }
            }

            if (_pointsNotGiven) {
                if ((_houseDone) && (_playerDone)) {
                    if (_playerScore == _houseScore) {
                        playerMoneyCoins -= winOrLoseAmount;
                        Debug.Log ("House Wins");
                        houseWonText.SetActive (true);
                        StartCoroutine (ResetGame ());
                        _pointsNotGiven = false;
                        _restartComplete = false;
                    }
                    if (_playerScore > _houseScore) {
                        playerMoneyCoins += winOrLoseAmount;
                        Debug.Log ("player Wins");
                        playerWonText.SetActive (true);
                        StartCoroutine (ResetGame ());
                        _pointsNotGiven = false;
                        _restartComplete = false;
                    } else {
                        if (_houseScore > _playerScore) {
                            playerMoneyCoins -= winOrLoseAmount;
                            Debug.Log ("House Wins");
                            houseWonText.SetActive (true);
                            StartCoroutine (ResetGame ());
                            _pointsNotGiven = false;
                            _restartComplete = false;
                        }
                    }
                } else if (_houseScore > 21) {
                    playerMoneyCoins += winOrLoseAmount;
                    Debug.Log ("player Wins");
                    playerWonText.SetActive (true);
                    StartCoroutine (ResetGame ());
                    _pointsNotGiven = false;
                    _restartComplete = false;
                } else if (_playerScore > 21) {
                    playerMoneyCoins -= winOrLoseAmount;
                    Debug.Log ("House Wins");
                    houseWonText.SetActive (true);
                    StartCoroutine (ResetGame ());
                    _pointsNotGiven = false;
                    _restartComplete = false;
                } else if (_houseScore == 21) {
                    playerMoneyCoins -= winOrLoseAmount;
                    Debug.Log ("House Wins");
                    houseWonText.SetActive (true);
                    StartCoroutine (ResetGame ());
                    _pointsNotGiven = false;
                    _restartComplete = false;
                } else if (_playerScore == 21) {
                    playerMoneyCoins += winOrLoseAmount;
                    Debug.Log ("player Wins");
                    playerWonText.SetActive (true);
                    StartCoroutine (ResetGame ());
                    _pointsNotGiven = false;
                    _restartComplete = false;
                }
            }
        }
    }

    public void GivePlayerCard () {
        Dealer.instance.DealPlayerCards ();
    }

    IEnumerator ResetGame () {

        yield return new WaitForSeconds (3f);

        _playerScore = 0;
        _houseScore = 0;

        _playerDone = false;
        _houseDone = false;

        _playerTurn = true;
        _houseTurn = false;
        _pointsNotGiven = true;

        Player.instance.playerInv.Clear ();
        House.instance.houseInv.Clear ();

        Player.instance.DestroyCards ();
        House.instance.DestroyCards ();

        yield return new WaitForSeconds (0.5f);

        Player.instance.playerCardInv.Clear ();
        House.instance.houseCardInv.Clear ();

        yield return new WaitForSeconds (2f);

        playerWonText.SetActive (false);
        houseWonText.SetActive (false);

        StartGame ();
        _restartComplete = true;

        yield return null;

    }

}