﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayCard : MonoBehaviour {

    public static DisplayCard instance;

    public static bool roundDone;
    public Card card;

    public SpriteRenderer rend;

    public Sprite cardFace;

    // Start is called before the first frame update
    private void Start () {

        roundDone = false;

        rend = GetComponent<SpriteRenderer> ();
        cardFace = card.cardFace;
        rend.sprite = cardFace;

    }


}
