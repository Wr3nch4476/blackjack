﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour {
    public static House instance;
    [SerializeField] public List<Card> houseInv = new List<Card> ();

    [SerializeField] public List<GameObject> houseCardInv = new List<GameObject> ();

    [SerializeField] public RectTransform houseCards;

    [SerializeField] public GameObject newHouseCard;

    public static int houseScore;
    [SerializeField] int editorHouseScore;

    public bool houseTurn = false;
    private void Awake () {
        instance = this;
    }

    private void Update () {
        editorHouseScore = houseScore;

        if (Game.instance._playerDone) {
            checkHouseScore ();
        }
    }

    private void checkHouseScore () {
        if ((houseScore < 16) && (Game.instance._playerDone) && (Game.instance._houseTurn)) {
            Dealer.instance.DealHouseCards ();

        } else if ((houseScore < 21) && (Game.instance._playerDone) && (Game.instance._houseTurn)) {
            Game.instance._houseDone = true;
            Game.instance._houseTurn = false;
        } else if ((houseScore > 21) && (Game.instance._playerDone) && (Game.instance._houseTurn)) {
            Game.instance._houseDone = true;
            Game.instance._houseTurn = false;
        }
    }

    public void UpdateHouseDeck (int index) {

        Card cardPlaceHolder = Dealer.instance.cardsNoShuffle[index];
        houseInv.Add (cardPlaceHolder);

        StartCoroutine (PauseABit ());

        DisplayCard CardSprite = newHouseCard.GetComponent<DisplayCard> ();
        CardSprite.card = cardPlaceHolder;

        StartCoroutine (PauseABit ());

        GameObject tempGameobject = Instantiate (newHouseCard, houseCards);
        houseCardInv.Add (tempGameobject);

        updateHouseScore ();

    }

    IEnumerator PauseABit () {

        yield return new WaitForSeconds (0.2f);

    }

    public void updateHouseScore () {

        int numberOfAces = 0;

        houseScore = 0;

        for (int i = 0; i < houseInv.Count; i++) {
            CardValue getCardValue = houseInv[i].cardNumber;

            if (getCardValue == CardValue.Ace) {
                ++numberOfAces;
            }
            if (getCardValue == CardValue.Two) {
                houseScore += 2;
            }
            if (getCardValue == CardValue.Three) {
                houseScore += 3;
            }
            if (getCardValue == CardValue.Four) {
                houseScore += 4;
            }
            if (getCardValue == CardValue.Five) {
                houseScore += 5;
            }
            if (getCardValue == CardValue.Six) {
                houseScore += 6;
            }
            if (getCardValue == CardValue.Seven) {
                houseScore += 7;
            }
            if (getCardValue == CardValue.Eight) {
                houseScore += 8;
            }
            if (getCardValue == CardValue.Nine) {
                houseScore += 9;
            }
            if (getCardValue == CardValue.Ten) {
                houseScore += 10;
            }
            if (getCardValue == CardValue.Jack) {
                houseScore += 10;
            }
            if (getCardValue == CardValue.Queen) {
                houseScore += 10;
            }
            if (getCardValue == CardValue.King) {
                houseScore += 10;
            }

            for (int h = 0; h < numberOfAces; h++) {
                if (h < numberOfAces - 1) {
                    houseScore += 1;
                } else {
                    if (houseScore < 11) {
                        houseScore += 11;
                    } else {
                        houseScore += 1;
                    }
                }
            }

            Game.instance._houseScore = houseScore;
        }

    } // end of updateHouseScore

    public void DestroyCards () {

        for (int i = 0; i < houseCardInv.Count; i++) {
            GameObject tempCard = houseCardInv[i];
            Destroy (tempCard);

        }

    }
}