There are some important details to take note of:

*    1. There are FOUR scripts that control the game. (House.cs, Player.cs, Game.cs, Dealer.cs)*
*    2. If you plan on using these scripts, you will need to use all the variables that are given.*
*    3. This version of Blackjack does not contain splitting, due to the chances of splitting are very rare and rounds go by quick in this version of the game*
*    4. Import your own art if you want to*

    
How these four sricpts work:

*    - At the start of the program, the system gives the player 100 point as money.*
*   - The system then checks if the game has run before to ensure that there will be no errors.*
*   - The system starts dealing cards to the player and then House.*
*   - The player have two buttons which controls the action that the player can take. (Hit and Stand)*
*   - As soon as the player clicks the button Stand will it become House's turn to take any action.*
*   - Note that if the player goes over 21 the player will lose without House taking action*
*   - When the player wins against house the player will gain 15 points (this can be changed on Game.cs or on the GameManager)*

    
Important Note: All scripts are connected and if a function or method or IEnummerator is changed it will be need to change on all scripts

Other details to take note of:

*   - All cards are ScriptableObjects (yes all 52 cards), all the information about the cards are created through the script Card.cs*
*   - The prefab that is connected to the scripts Player.cs and House.cs is the object that allows the player to see what card he or she has.*
*   - On the prefab there is a script named "displayCard" which is the medium that adds the visuals to the object*
*   - All the cards are on a list on the GameObject named "Dealer", the script named Dealer.cs is the script that gives the player and house their cards*
*   - The player will see both the cards of the dealer through out the game*


